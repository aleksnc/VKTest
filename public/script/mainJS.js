VK.init({
    apiId: 6384895
});


function addImage(id_albom) {
    $('.main_slider').empty()
    $.ajax({
        url: "https://api.vk.com/method/photos.get?v=5.52&group_id=14195074&album_id="+id_albom+"&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0",
        dataType: "jsonp",
        success: function (data) {
            console.log(data.response);

            for (var prop in data.response.items) {
                 var id = data.response.items[prop]['thumb_id'];
                 var name = data.response.items[prop]['text'];
                 var img = data.response.items[prop]['photo_604'];

                var e = $('.sliderImg.parent').clone().removeClass('parent');
                e.find('img').attr({
                    'alt': name,
                    'src': img
                });
                $('.main_slider').prepend(e);
             }

            slickSlider();
        }
    })
}

function addText(about) {
    $.ajax({
        url: "https://api.vk.com/method/board.getComments?v=5.52&group_id=14195074&topic_id="+about+"&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0",
        dataType: "jsonp",
        success: function (data) {
            console.log(data.response);
            var text =data.response.items[0]['text']
            text.replace("\n1","|<br/>") 
            text.replace("\n2","|<br/>") 
            text.replace("\nc","|<br/>") 
            text.replace("\n","|<br/>") 
            $('.board_aboutUs').empty().text(text);


        }
    })
}

function slickSlider() {
    $('.main_slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        margin: 10,
        variableWidth: true
    });
}

$(document).ready(function () {
    if (location.pathname === '/VKTest/' || location.pathname === '/VKTest/index.html') {
        var savedUser = JSON.parse(localStorage.getItem("myPage"));
        if(savedUser===null){
            location.href = '/VKTest/admin.html';
        }
        addImage(savedUser.slider);
        addText(savedUser.about);

    } else {
        if (location.pathname !== '/VKTest/admin.html') {
            location.href = '/VKTest/admin.html';
        } else {
            $('.getAll_js').click(function (e) {
                var alboms = [];
                $('.listBlog__blogs').empty()
                $.ajax({
                    url: "https://api.vk.com/method/board.getTopics?v=5.52&group_id=14195074&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0",
                    dataType: "jsonp",
                    success: function (data) {
                        console.log(data.response);

                        for (var prop in data.response.items) {
                            console.log("obj." + prop + " = " + data.response.items[prop]['title']);
                            var id = data.response.items[prop]['id'];
                            var name = data.response.items[prop]['title'];
                            var e = $('.listBlog .listBlog__block.parent').clone().removeClass('parent');
                            e.find('input[type="radio"]').attr('id', 'r' + id).data('val', id).prop({'checked': false}).siblings('label').attr(
                                {
                                    'for': 'r' + id,
                                    'value': id
                                });
                            e.find('input[type="checkbox"]').attr('id', 'c' + id).data('val', id).siblings('label').attr({
                                'for': 'c' + id,
                                'value': id
                            });
                            e.find('.listBlog__nameBlog').empty().html(name);
                            $('.listBlog .listBlog__blogs').prepend(e);
                        }
                    }
                })
                $.ajax({
                    url: "https://api.vk.com/method/photos.getAlbums?v=5.52&&need_covers=1&group_id=14195074&access_token=b9568949b9568949b956894966b937e5b6bb956b9568949e3c7f1667a7a376956381ce0",
                    dataType: "jsonp",
                    success: function (data) {
                        console.log(data.response);

                        for (var prop in data.response.items) {
                            console.log("obj." + prop + " = " + data.response.items[prop]['id']);
                            var id = data.response.items[prop]['id'];
                            var name = data.response.items[prop]['title'];
                            var img = data.response.items[prop]['thumb_src'];
                            console.log(img);
                            var e = $('.listAlbom .listBlog__block.parent').clone().removeClass('parent');
                            e.find('input[type="radio"]').attr('id', 'r' + id).data('val', id).prop({'checked': false}).siblings('label').attr(
                                {
                                    'for': 'r' + id,
                                    'value': id
                                });
                            e.find('input[type="checkbox"]').attr('id', 'c' + id).data('val', id).siblings('label').attr({
                                'for': 'c' + id,
                                'value': id
                            });
                            e.find('img').attr({
                                'alt': name,
                                'src': img
                            });
                            e.find('.listBlog__nameBlog').empty().html(name);
                            $('.listAlbom .listBlog__blogs').prepend(e);
                        }
                    }
                })


                $('.saveBlog__js').slideDown();
            });

            $('.saveBlog__js').click(function () {
                var blog = [];
                var about = $('.listBlog').find('input[name=aboutus]:checked').data('val');
                var album = $('.listAlbom').find('input[name=mainSlider]:checked').data('val');

                $('.listBlog').find('input[name=blog]:checked').each(function () {
                    blog.push($(this).data('val'))
                })

                var myPage = {
                    about: about,
                    slider: album,
                    blog: blog,
                };

                localStorage.setItem("myPage", JSON.stringify(myPage));
                location.href='/VKTest/';
            })
        }

    }
})